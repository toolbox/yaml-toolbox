FROM containers.ligo.org/toolbox/igwn-toolbox:el9

RUN dnf -y install ruby && \
    dnf clean all

RUN gem install yamllint
